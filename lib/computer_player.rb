class ComputerPlayer
  attr_accessor :name, :board, :mark

  def initialize(name, mark=:O)
    @name = name
    @mark = mark
  end #initialize

  def display(board)
    @board = board
  end #display

  def get_move
    for i in (0...board.grid.length)
      for j in (0...board.grid.length)
        return [i, j] if winning_move?([i, j])
      end
    end
    return random_move
  end #get_move

  def winning_move?(pos)
    #After all the test a quick look up and Ruby does not do deep copies
    #Goddman it. I'll have to revert my changes then
    res = false
    if board.empty?(pos)
      board.place_mark(pos, mark)
      res = true if board.winner
      board.place_mark(pos, nil)
    end
    res
  end #winning_move?

  def random_move
    pos = [rand(board.grid.length), rand(board.grid.length)]
    until board.empty?(pos)
      pos = [rand(board.grid.length), rand(board.grid.length)]
    end
    pos
  end #random_move
end #class ComputerPlayer
