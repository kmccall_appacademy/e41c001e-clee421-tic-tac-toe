require_relative 'board'
require_relative 'human_player'
require_relative 'computer_player'

class Game
  attr_accessor :board, :player_one, :player_two, :current_player, :mark

  def initialize(player_one, player_two)
    @player_one = player_one
    @player_two = player_two
    @current_player = player_one
    @board = Board.new
    @mark = mark
  end #initialize

  #which calls `play_turn` each time through a loop until the game is over
  def play

  end

  def current_player
    current_player = player_one
  end

  def switch_players!
    @player_one, @player_two = @player_two, @player_one
  end

  #Handles the logic for a single turn
  def play_turn
    current_player.display(board)
    new_move = current_player.get_move
    board.place_mark(new_move, current_player.mark)
    switch_players!
  end
end #class Game
