class HumanPlayer
  attr_accessor :name, :board, :mark

  def initialize(name, mark=:X)
    @name = name
  end #initialize

  def get_move
    print "Where to place mark (e.g. 0, 0): "
    input = gets.chomp
    input = input.delete(',')
    x, y = input.split
    [x.to_i, y.to_i]
  end #get_move

  def display(board)
    board.display
  end #display
end
