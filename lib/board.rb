class Board
  attr_reader :grid

  def initialize(init_val=[[nil, nil, nil], [nil, nil, nil], [nil, nil, nil]])
    @grid = init_val
  end #initialize

  def display
    disp_arr = []
    grid.each do |row|
      row_arr = []
      row.each do |pos|
        pos ? disp = row_arr << pos.to_s.center(5, " ") : row_arr << "     "
      end
      disp_arr << row_arr.join('|')
    end
    puts disp_arr.join("\n_________________\n\n")
  end #display

  # Takes a position such as `[0, 0]` and a mark such as :X
  #  as arguments. It should throw an error if the position isn't empty.
  def place_mark(pos, mark)
    grid[pos[0]][pos[-1]] = mark
  end #place_mark

  def get_mark(pos)
    grid[pos[0]][pos[-1]]
  end #get_mark

  # Takes a position as an argument
  def empty?(pos)
    !(grid[ pos[0] ][ pos[-1] ])
  end #empty?

  # Return a mark
  # Assumers marks to be :X or :O, otherwise would have to store 2 marks
  def winner
    return :X if row_win?(:X)
    return :O if row_win?(:O)
    return :X if col_win?(:X)
    return :O if col_win?(:O)
    return :X if diag_win?(:X)
    return :O if diag_win?(:O)
    return nil
  end #winner

  def row_win?(mark)
    grid.any? {|row| row.all?{|m| m == mark}}
  end #row_win

  def col_win?(mark)
    rotate = grid.transpose
    rotate.any? {|row| row.all?{|m| m == mark}} #Refactor?
  end #col_win

  def diag_win?(mark)
    return true if left_diag?(mark)
    return true if right_diag?(mark)
    return false
  end #diag_win

  def left_diag?(mark)
    for i in (0...grid.length)
      return false unless grid[i][i] == mark
    end
    true
  end #left_diag?

  def right_diag?(mark)
    for i in (0...grid.length)
      return false unless grid[i][-(i+1)] == mark
    end
    true
  end #right_diag?

  #Return true or false
  def over?
    full = grid.map {|row| row.compact.length}.uniq.all? {|l| l == 3}
    full || winner
  end #over?
end #class Board
